section .text

 
; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, 60
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
    .loop:
	cmp byte[rdi+rax], 0x0
	je .ext
	inc rax
        jmp .loop
    .ext:
	ret
; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    push rdi
    call string_length
    pop  rdi
    mov  rdx, rax
    mov  rsi, rdi
    mov  rax, 1
    mov  rdi, 1
    syscall
    ret



; Принимает код символа и выводит его в stdout
print_char:
    push rdi
    mov  rdx, 1
    mov  rsi, rsp
    mov  rax, 1
    mov  rdi, 1
    syscall
    pop rdi
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov  rdi, 0xA
    call print_char
    ret

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    mov rax, rdi
    mov rcx, 10
    mov rsi, rsp
    sub rsp, 32
    dec rsi
    mov byte[rsi], 0
    .loop:
	dec rsi
	xor rdx, rdx
	div rcx
	add rdx, '0'
	mov [rsi], dl
	test rax, rax
	jne .loop
    mov rdi, rsi
    call print_string
    add rsp, 32
    ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    cmp rdi, 0
    jge .positive
    push rdi
    mov rdi, '-'
    call print_char
    pop rdi
    neg rdi
    .positive:
	call print_uint
    ret

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    .loop:
	mov al, [rdi]
	cmp al, [rsi]
	jne .no
	cmp al, 0
	je .yes
	inc rdi
	inc rsi
	jne .loop
    .yes:
	mov rax, 1
	ret
    .no:
	xor rax, rax
	ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
   xor rax, rax
   push rax
   xor rdi, rdi
   mov rsi, rsp
   mov rdx, 1
   syscall
   test rax, rax
   jz .end
   cmp rax, -1
   jz .end
   pop rax
   ret
   .end:
   xor rax, rax
   pop rcx
   ret

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор
read_word:
    xor rax, rax
    xor rdx, rdx
    push r12
    push r13
    push r14
    mov r12, rdi
    mov r13, rdx
    mov r14, rsi
    .check:
	call read_char
	cmp al, 0x20
	je .check
	cmp al, 0x9
	je .check
	cmp al, 0xA
	je .check
    .loop:
	cmp al, 0x0
	jz .end
	cmp r14, r13
	je .error
	cmp al, 0x20
	je .end
	cmp al, 0x9
	je .end
	cmp al, 0xA
	je .end
	mov byte[r12 + r13], al
	call read_char
	inc r13
	jmp .loop
    .end:
	cmp r13, r14
	je .error
	mov byte[r12 + r13], 0
	mov rax, r12
	mov rdx, r13
	pop r14
	pop r13
	pop r12
	ret
    .error:
	mov rax, 0
	pop r14
	pop r13
	pop r12
	ret


; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rdx, rdx
    xor rax, rax
    xor rcx, rcx
    .loop:
	movzx rcx, byte[rdi+rdx]
        test rcx, rcx
	jz .ext
	cmp rcx, '0'
        jl .ext
	cmp rcx, '9'
	jg .ext
	sub rcx, '0'
	imul rax, 10
        add rax, rcx
        inc rdx
        jmp .loop
     .ext:
	ret


; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    xor rdx, rdx
    cmp byte[rdi], `-`
    je .minus
    cmp byte[rdi], `+`
    je .plus
    call parse_uint
    ret
    .minus:
    inc rdi
    	call parse_uint
    	test rdx, rdx
    	neg rax
    	inc rdx
	ret
    .plus:
        inc rdi
        call parse_uint
	inc rdx
        ret 

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    push r12
    push rdi
    push rsi
    push rdx
    call string_length
    pop rdx
    pop rsi
    pop rdi
    inc rax
    cmp rax, rdx
    jg .err
    push rax
    mov r12, rax
    inc r12
    xor rax, rax
    xor rcx, rcx
    .loop:
	mov al, [rdi+rcx]
	mov [rsi+rcx], al
	inc rcx
	dec r12
        test r12, r12
	jnz .loop
        pop rax
        pop r12
        ret
    .err:
	xor rax,rax
    pop r12
	ret

